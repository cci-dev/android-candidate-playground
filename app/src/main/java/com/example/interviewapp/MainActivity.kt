package com.example.interviewapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView


/**
 * This is just a simple application that performs "some" function and displays
 * the output of that function in a text box.
 *
 * Feel free to extend this or bring your own skeleton project to the interview.
 * As long as we can see your thought process while working out a problem,
 * and some way to see the output of what code you write
 *
 * TODO: replace this comment block with the instructions provided in your interview.
 * TODO: Make sure you are able to compile and run this code (or whatever code you've elected to bring instead) before the interview
 *
 * If for some reason you're unable to build and run this program or bring an alternate,
 * just use any text editor you like and we'll try work through the evaluation with that.
 */

const val sampleInputData =
    "We might provide you with some sample data on which to perform some operation"
const val sampleInputData2 = "sampleInputData2"
const val sampleInputData3 = "sampleInputData3"

class MainActivity :
    AppCompatActivity(),
    View.OnClickListener {

    private var presentButton: Button? = null
    private var textDisplay: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presentButton = findViewById(R.id.present)
        textDisplay = findViewById(R.id.display)
        presentButton?.setOnClickListener(this)
    }

    /**
     * Performs the operation on all of the input variables and writes the concatenated
     * results to the TextView
     */
    private fun performOperationAndDisplayOutput(vararg input: Any) {
        var outputString = ""
        input.forEach {
            outputString += (performOperation(it).toString())
            outputString += ("\n\n")
        }
        textDisplay?.text = outputString
    }

    /**
     * We'll likely be changing the name and data types of this function depending on the parameters
     * of the interview
     */
    private fun performOperation(input: Any): Any {
        return "$input: Operation complete"
    }

    override fun onClick(p0: View?) {
        val id = p0?.id ?: return
        when (id) {
            R.id.present -> performOperationAndDisplayOutput(
                sampleInputData,
                sampleInputData2,
                sampleInputData3
            )
            else -> {
            }
        }
    }
}